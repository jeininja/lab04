//Jei wen Wu - 1842614

package geometry;

public class Circle implements Shape {

	private double radius;
	
	public Circle (double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	@Override
	public double getArea() {
		double area = Math.PI*2*this.radius;
		return area;
	}

	@Override
	public double getPerimeter() {
		double perimeter = 2*Math.PI*radius;
		return perimeter;
	}
}
