//Jei wen Wu - 1842614

package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(3.3, 9.9);
		shapes[1] = new Circle(4.4);
		shapes[2] = new Square(6.6);
		shapes[3] = new Rectangle(2.2, 7.7);
		shapes[4] = new Square(5.5);

		for(int i=0;i<shapes.length;i++) {
			System.out.println(shapes[i].getArea());
			System.out.println(shapes[i].getPerimeter()+"\n");
		}
	}

}
