//Jei wen Wu - 1842614

package geometry;

public class Rectangle implements Shape{

	double length;
	double width;
	
	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
	}
	
	public double getLength() {
		return this.length;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	@Override
	public double getArea() {
		double area = width*length;
		return area;
	}

	@Override
	public double getPerimeter() {
		double perimeter = width*2+length*2;
		return perimeter;
	}

}
