//Jei wen wu - 1842614

package geometry;

public interface Shape {

	double getArea();
	
	double getPerimeter();
}
