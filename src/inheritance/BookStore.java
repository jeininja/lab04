//Jei wen Wu - 1842614

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] books = new Book[5];
		books[0] = new ElectronicBook("Robots","Anne Droid", 2345.1);
		books[1] = new Book("How hot is the sun?", "Jhon Son");
		books[2] = new ElectronicBook("Danger!", "Luke Out", 2305.62);
		books[3] = new Book("I Win!", "U. Lose");
		books[4] = new ElectronicBook("Downpour!", "Wayne Dwops", 2444.44);
		
		for(int i=0;i<books.length;i++) {
			System.out.println(books[i]);
		}
	}
}
