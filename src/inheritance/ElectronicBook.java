//Jei wen Wu - 1842614

package inheritance;

public class ElectronicBook extends Book {

	private double numberBytes;
	
	public ElectronicBook(String title, String author, double numberBytes) {
		super(title, author);
		this.numberBytes = numberBytes;
	}
	
	public double getNumberBytes(){
		return this.numberBytes;
	}
	
	@Override
	public String toString() {
		String s = super.toString()+" Size: "+getNumberBytes()+" bytes";
		return s;
	}
	
}
